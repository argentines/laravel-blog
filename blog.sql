-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3307
-- Время создания: Май 16 2019 г., 13:09
-- Версия сервера: 8.0.12
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `created_at`, `updated_at`) VALUES
(1, 'Trever Aufderhar DDS', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(2, 'Easton Ratke II', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(3, 'Stanford Ledner', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(4, 'Tremayne Lindgren', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(5, 'Dr. Lura Stokes', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(6, 'Reva Bahringer', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(7, 'Arne Dickens', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(8, 'Prof. Ebba Hudson', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(9, 'Tony Hirthe', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(10, 'Jennie Kertzmann', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(32, 'Abricosd updated', '2019-05-12 06:14:12', '2019-05-12 11:54:26'),
(33, 'Mandarina', '2019-05-12 12:12:01', '2019-05-12 12:12:01');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_10_153804_create_categories_table', 2),
(4, '2019_05_10_154540_create_posts_table', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `cat_id`, `user_id`, `comment_id`, `title`, `description`, `photo`, `created_at`, `updated_at`) VALUES
(1, 10, '6', '10', 'Sed cum in est numquam voluptatem minima.', 'Pariatur quis et corporis accusamus. Rem dignissimos aut maxime magnam. Velit omnis deleniti dolores. Saepe laboriosam autem et quos ipsa architecto.', 'photo_1.jpg', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(2, 8, '7', '3', 'Rerum voluptas dolor voluptas deserunt harum.', 'Laborum nostrum doloremque dicta dolores dolores enim at quia. Laudantium non non odit molestias nihil id nobis omnis. Tempora ratione quis pariatur debitis accusantium deleniti rerum. Corporis est dolorum dolorem ullam.', 'photo_11.jpg', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(3, 9, '5', '1', 'Non ut reprehenderit est ipsam commodi a velit.', 'Autem ea officiis non eos. Est voluptas error animi maxime magnam voluptatem. Deserunt et voluptas labore architecto. Enim mollitia ut molestias error sint aut rerum molestias.', 'photo_2.jpg', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(4, 4, '10', '10', 'Non et quo voluptatibus.', 'Occaecati laborum animi occaecati eum tempore. Quam voluptas vero nemo cum. Iste laborum laborum excepturi vero.', 'photo_3.jpg', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(5, 1, '6', '1', 'Quia tempora laudantium fugit alias cum officiis aut.', 'Itaque quo est sunt. Quisquam similique ex ut ea dolores. Quia voluptatum repudiandae porro. Quo ea quis et facilis iure eos minus.', 'photo_4.jpg', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(6, 7, '4', '7', 'Consequatur eum cumque harum incidunt sed.', 'Quas voluptas provident facilis perferendis non maxime aperiam odit. Iure rem deleniti ab aperiam enim dicta. Est necessitatibus rerum consequatur et consequatur eligendi itaque. In aut fuga architecto praesentium enim.', 'photo_5.jpg', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(7, 7, '2', '7', 'Non omnis consequuntur consectetur odio nostrum.', 'Officiis sint vel tempore impedit aliquid harum occaecati voluptatibus. Dolor quis aliquam voluptatem quam impedit. Amet qui consequuntur fugiat. Et vel sit a voluptatem quasi perferendis vel.', 'photo_6.jpg', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(8, 8, '5', '1', 'Qui ea tempora facilis quibusdam corporis placeat qui.', 'Voluptas ut soluta magnam labore voluptatum ut. Consequuntur dolorum in similique veniam ut rerum maxime. Quae voluptatibus voluptatem ut qui. Aspernatur dolor quod voluptate aut. Et blanditiis ea nihil repellat rem quis repellendus.', 'photo_7.jpg', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(9, 6, '1', '5', 'Et rerum tenetur aut necessitatibus quisquam.', 'Magnam quod modi reiciendis voluptas. Veniam et et aut. Excepturi laboriosam tempore et est inventore laudantium hic. Vitae esse assumenda voluptatibus iste. Nobis voluptatem id ut dolor voluptas.', 'photo_8.jpg', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(10, 2, '1', '8', 'Reprehenderit quibusdam recusandae', 'Exercitationem repudiandae dolorum eveniet deleniti perspiciatis qui aut. Ad optio perspiciatis ut modi voluptate. Magnam nihil labore quis perspiciatis. Quaerat animi voluptas omnis tempora qui qui temporibus. A enim cum aliquid nostrum pariatur qui.', '1557956807.jpeg', '2019-05-10 15:25:06', '2019-05-15 19:46:47'),
(14, 10, '1', NULL, 'Reprehenderit quibusdam', 'Exercitationem repudiandae dolorum eveniet deleniti perspiciatis qui aut. Ad optio perspiciatis ut modi voluptate. Magnam nihil labore quis perspiciatis. Quaerat animi voluptas omnis tempora qui qui temporibus. A enim cum aliquid nostrum pariatur qui.', '1557956497.jpeg', '2019-05-15 19:41:37', '2019-05-15 19:41:37'),
(15, 10, '1', NULL, 'Reprehenderit quibusdam recusandae', 'Exercitationem repudiandae dolorum eveniet deleniti perspiciatis qui aut. Ad optio perspiciatis ut modi voluptate. Magnam nihil labore quis perspiciatis. Quaerat animi voluptas omnis tempora qui qui temporibus. A enim cum aliquid nostrum pariatur qui.', '1557956552.jpeg', '2019-05-15 19:42:32', '2019-05-15 19:42:32');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Sergio', 'srkuguk@gmail.com', NULL, '$2y$10$iO1qFg1KN4Yn1P9GCBoCHOTPHxeh7y/wsyWA/43WvfZaK2QYvaKca', NULL, '2019-05-10 11:23:48', '2019-05-10 11:23:48'),
(2, 'Sterling Feeney', 'lonnie.flatley@example.com', '2019-05-10 15:25:06', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'thcRW4ogQU', '2019-05-10 15:25:06', '2019-05-10 15:25:06'),
(3, 'Baron Upton', 'sschultz@example.net', '2019-05-10 15:25:06', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ksnbeu3Url', '2019-05-10 15:25:09', '2019-05-10 15:25:09'),
(4, 'Eliezer Beatty', 'kutch.chauncey@example.net', '2019-05-10 15:25:06', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'rcQV5FYuUd', '2019-05-10 15:25:09', '2019-05-10 15:25:09'),
(5, 'Kameron Kirlin', 'qschultz@example.net', '2019-05-10 15:25:06', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UycgfNKaoE', '2019-05-10 15:25:09', '2019-05-10 15:25:09'),
(6, 'Tod Murray', 'michale63@example.net', '2019-05-10 15:25:06', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'grD2RM9lEz', '2019-05-10 15:25:09', '2019-05-10 15:25:09'),
(7, 'Efren Schultz', 'margie.hane@example.net', '2019-05-10 15:25:06', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'BzjPQ7Dv6Y', '2019-05-10 15:25:09', '2019-05-10 15:25:09'),
(8, 'Deborah Schuster', 'alexis38@example.net', '2019-05-10 15:25:06', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '5bzBrLj3W9', '2019-05-10 15:25:09', '2019-05-10 15:25:09'),
(9, 'Lorena Sauer', 'jweissnat@example.org', '2019-05-10 15:25:06', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'p1uuM4UFBe', '2019-05-10 15:25:09', '2019-05-10 15:25:09'),
(10, 'Prof. Nicolette Abshire', 'murazik.gay@example.org', '2019-05-10 15:25:06', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7WVXmGaASJ', '2019-05-10 15:25:09', '2019-05-10 15:25:09'),
(11, 'Oleta Okuneva Jr.', 'lreichel@example.net', '2019-05-10 15:25:06', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Q5lzWtoZJx', '2019-05-10 15:25:09', '2019-05-10 15:25:09');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
