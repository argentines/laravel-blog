<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryContrller extends Controller
{

    public function getAllCategories(){

        $categories = Category::all();
        return response()->json([
            'categories' => $categories
        ],200);
    }

    public function addCategory(Request $request){
        $this->validate($request, [
            'cat_name' => 'required|regex:/^[a-zA-Z]+$/u|min:2|max:50'
            ]);

        $category = new Category();
        $category->cat_name = $request->cat_name;
        $save = $category->save();

        if(!$save){
            return ['Error' => 'Category don`t saved'];
        }
        return ['message' => '200 OK'];
    }

    public function getCategoryForUpdate($id){

        $category = Category::find($id);
        return response()->json([
            'category' => $category
        ], 200);
    }

    public function updateCategory(Request $request, $id){

        $this->validate($request, [
            'cat_name' => 'required|regex:/^[a-zA-Z\-\_ ]+$/u|min:2|max:50'
            ]);

        $category = Category::find($id);
        $category->cat_name = $request->cat_name;
        $category->save();
    }

    public function removeCategory($id){

        $category = Category::find($id);
        $category->delete();
    }
}
