import AdminHome from './components/admin/AdminHome.vue';
//Category
import CategoryList from './components/admin/category/CategoryList.vue';
import AddCategory from './components/admin/category/AddCategory.vue';
import EditCategory from './components/admin/category/EditCategory.vue';
//Post
import PostList from './components/admin/post/PostList.vue';
import AddPost from './components/admin/post/AddPost.vue';
import EditPost from './components/admin/post/EditPost.vue';

//Front Page
import PublicHome from './components/public/PublicHome.vue';
import SinglPost from './components/public/blog/SinglPost.vue';

export const routes = [
    //Category
    {
        path: '/home',
        name: 'Home',
        component: AdminHome
    },
    {
        path: '/category-list',
        name: 'Category List',
        component: CategoryList
    },
    {
        path: '/add-category',
        name: 'Add Category',
        component: AddCategory
    },
    ,
    {
        path: '/edit-category/:categoryid',
        name: 'Edit Category',
        component: EditCategory
    },
    //Post
    {
        path: '/post-list',
        name: 'Post List',
        component: PostList
    },
    {
        path: '/add-post',
        name: 'Add Post',
        component: AddPost
    },
    {
        path: '/edit-post/:postid',
        name: 'Edit Post',
        component: EditPost
    }
    //Home Pages
    ,
    {
        path: '/',
        name: 'Home Page',
        component: PublicHome
    },
    {
        path: '/single-post/:id',
        name: 'Singel Post',
        component: SinglPost
    },
    {
        path: '/categories/:id',
        component: SinglPost
    }
]
