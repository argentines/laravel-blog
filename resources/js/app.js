require('./bootstrap');
window.Vue = require('vue');

import VueRouter from 'vue-router';
import { routes } from './routes';
import { Form, HasError, AlertError } from 'vform';
import { filter } from './filter';
import Swal from 'sweetalert2';
import Vuex from 'vuex';
import storeData from "./components/store/index";
import Editor from 'v-markdown-editor';
import 'v-markdown-editor/dist/index.css';

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(Editor);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('admin-main', require('./components/admin/AdminMaster.vue').default);
Vue.component('home-main', require('./components/public/PublicMaster.vue').default);
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

window.Form = Form;
window.swal = Swal;
window.toast = toast;

const store = new Vuex.Store(
    storeData
);

const router = new VueRouter({
    routes: routes,
    mode:'history'
})

const app = new Vue({
    el: '#app',
    router,
    store
});
