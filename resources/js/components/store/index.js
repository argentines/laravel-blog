export default{

    state:{
        category:[],
        post:[],
        blogpost:[],
        singlpost:[],
        categories:[],
        latestpost:[]
    },
    getters:{
        getCategory(state){
            return state.category;
        },
        getAllPost(state){
            return state.post;
        },
        getBlogPost(state){
            return state.blogpost;
        },
        getPostById(state){
            return state.singlpost;
        },
        getAllCategories(state){
            return state.categories;
        },
        getLatestPost(state){
            return state.latestpost;
        },
        getPostByCatId(state){
            //return state.latestpost;
        }
    },
    actions:{
        allCategories(context){
          axios.get('/categories')
                .then((response) => {
                    context.commit('categoreis', response.data.categories);
                });
        },
        getAllPost(context){
          axios.get('/post')
            .then((response) => {
                context.commit('allpost', response.data.posts);
            });
        },
        getBlogPost(context){
            axios.get('/blogposts')
              .then((response) => {
                  context.commit('getBlogPost', response.data.posts);
              });
        },
        getPostById(context, payload){
            axios.get('/singlpost/'+payload)
                .then((response) => {
                    context.commit('setPostById', response.data.post);
                });
        },
        getCategories(context, payload){
            axios.get('/categories')
                .then((response) => {
                    context.commit('setAllCategories', response.data.categories);
                });
        },
        latestPost(context){
            axios.get('/latestpost')
                .then((response)=>{
                    context.commit('setLatestPost',response.data.posts)
                })
        },
        getPostByCatId(context, payload){
            axios.get('/singlpost/'+payload)
                .then((response) => {
                    context.commit('setPostByCatId', response.data);
                });
        }
    },
    mutations:{
        categoreis(state, data){
            return state.category = data;
        },
        allpost(state, payload){
            return state.post = payload;
        },
        getBlogPost(state, payload){
            return state.blogpost = payload;
        },
        setPostById(state, payload){
            return state.singlpost = payload;
        },
        setAllCategories(state, payload){
            return state.categories = payload;
        },
        setLatestPost(state, payload){
            return state.latestpost = payload;
        },
        setPostByCatId(state, payload){
            //return state.latestpost = payload;
        }
    }
}
