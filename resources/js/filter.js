import Vue from 'vue';
import moment from 'moment';

Vue.filter('timeformat', (date) => {
    return moment(date).format("MMM Do YYYY");
});

Vue.filter('sortlength', (text, length, suffix) => {
    return text.substring(0, length)+suffix;
});
