<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('public/index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Category Admin
Route::get('/categories', 'CategoryContrller@getAllCategories');
Route::get('/category/{id}', 'CategoryContrller@removeCategory');
Route::get('/editecategory/{id}', 'CategoryContrller@getCategoryForUpdate');
Route::post('/update-category/{id}', 'CategoryContrller@updateCategory');
Route::post('/add-category', 'CategoryContrller@addCategory');

//Post Admin
Route::post('/add-post', 'PostController@addPost');
Route::post('/update-post/{id}', 'PostController@updatePost');
Route::get('/delete-post/{id}', 'PostController@deletePost');
Route::get('/post', 'PostController@getAllPost');
Route::get('/post/{id}', 'PostController@getOnePost');

//Blog
Route::get('/singlpost/{id}', 'BlogController@getPostById');
Route::get('/blogposts', 'BlogController@getAllBlogPost');
Route::get('/categories', 'BlogController@getAllCategories');
Route::get('/latestpost', 'BlogController@getLatestPost');

Route::get('/{enypath}', 'HomeController@index')->where('path', '.*');
